import matplotlib
from simulator import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pickle
from math import acos, sqrt
import sys

matplotlib.use("TkAgg")



def norm(vector):
	val = 0
	for x in vector:
		val+=x**2
	return sqrt(val)

def dotprod(vector1,vector2):
	val =0
	for i,x in enumerate(vector1):
		val+= x*vector2[i]
	return val

def plot_vector(ax,endpoint,split = 50):
	xa,ya,za = [],[],[]
	xe,ye,ze = endpoint
	for i in range(split):
		xa.append(float(xe)*i/split)
		ya.append(float(ye)*i/split)
		za.append(float(ze)*i/split)
	ax.plot(xa,ya,za)

def plot_dist(ax,data):
	ax.hist(data,bins=10**2,density = False)
	
def single_particle_test():
	simple_world = World(1000,1000,1000,[],[])
	photons = [Photon(simple_world,(0,0,5),(20,10,0))]*3
	atoms = [Atom(simple_world,(10,10,5),1)]

	for x in photons:
		simple_world.create_photon(x)
	for x in atoms:
		simple_world.create_atom(x)

	for i in range(3000):
		simple_world.inct()
	simple_world.stop_sim()

	fig = plt.figure()
	ax = fig.add_subplot(111,projection='3d')
	for i in range(3):
		xlog = [t[0] for t in simple_world.photons[i].location_log]
		ylog = [t[1] for t in simple_world.photons[i].location_log]
		zlog = [t[2] for t in simple_world.photons[i].location_log]
		ax.scatter(xlog,ylog,zlog)

	plt.show()

def diamond(showoff=0, NUM_PHOTONS = 500):
	T_CNT = 10**5
	simple_world = World(20,20,20,[],[])
	initial_velocity = (-10,-10,-10)
	photons = [Photon(simple_world,(20,20,20),initial_velocity)]*NUM_PHOTONS
	a=10
	locations = []
	fp = open("diamond.txt","r")
	for l in fp:
		locations.append(eval(l))
	atoms =[]
	for x in locations:
		atoms.append(Atom(simple_world,x,5))

	for x in photons:
		simple_world.create_photon(x)
	for x in atoms:
		simple_world.create_atom(x)

	for i in range(T_CNT):
		if i%10 == 0:
			print("[*] Simulation {}% complete".format(float(i)/T_CNT*100))
		simple_world.inct()
	simple_world.stop_sim()
	print ("[+] Simulation complete.")
	
	
	if showoff == 1:
		fig = plt.figure()
		ax = fig.add_subplot(111,projection='3d')
		for i in range(NUM_PHOTONS):
			xlog = [t[0] for t in simple_world.photons[i].location_log]
			ylog = [t[1] for t in simple_world.photons[i].location_log]
			zlog = [t[2] for t in simple_world.photons[i].location_log]
			ax.plot(xlog,ylog,zlog)
		

		xlog = [t._x for t in simple_world.atoms]
		ylog = [t._y for t in simple_world.atoms]
		zlog = [t._z for t in simple_world.atoms]

		ax.scatter(xlog,ylog,zlog)
		plt.show()
		return
	elif showoff == 2:
		fig = plt.figure()
		ax = fig.add_subplot(111,projection='3d')
		for x in simple_world.photons:
			vec = x._vx,x._vy,x._vz
			plot_vector(ax,vec)
		plt.show()

	elif showoff == 3:
		fig = plt.figure()
		ax = fig.add_subplot(111)
		data = []
		for x in simple_world.photons:
			vec = x._vx,x._vy,x._vz
			data.append(dotprod(vec,initial_velocity)/(norm(vec)*norm(initial_velocity)))
		plot_dist(ax,data)
		plt.show()

def graphite(showoff=0, NUM_PHOTONS = 100):
	T_CNT = 10**5
	simple_world = World(20,20,20,[],[])
	initial_velocity = (-10,-10,-10)
	photons = [Photon(simple_world,(20,20,20),initial_velocity)]*NUM_PHOTONS
	d,x,y,h = 1.0,3,1,5
	#핵간 거리, 너비 스케일, 층수, 층가 거리
	locations = []
	fp = open("graphite.txt","r")
	for l in fp:
		locations.append(eval(l))
	atoms =[]
	for loc in locations:
		atoms.append(Atom(simple_world,loc,1))

	for photon in photons:
		simple_world.create_photon(photon)
	for atom in atoms:
		simple_world.create_atom(atom)

	for i in range(T_CNT):
		if i%10 == 0:
			sys.stdout.write("\r")
			sys.stdout.write("[*] Simulation {0:.2f}% complete".format(float(i)/T_CNT*100))
		simple_world.inct()
	sys.stdout.write("\n")
	print ("[+] Simulation complete.")
	
	
	if showoff == 1:
		fig = plt.figure()
		ax = fig.add_subplot(111,projection='3d')
		for i in range(NUM_PHOTONS):
			xlog = [t[0] for t in simple_world.photons[i].location_log]
			ylog = [t[1] for t in simple_world.photons[i].location_log]
			zlog = [t[2] for t in simple_world.photons[i].location_log]
			ax.scatter(xlog,ylog,zlog,s=0.1)
		

		xlog = [t._x for t in simple_world.atoms]
		ylog = [t._y for t in simple_world.atoms]
		zlog = [t._z for t in simple_world.atoms]

		ax.scatter(xlog,ylog,zlog, s = 1)
		plt.show()
		return

	elif showoff == 2:
		fig = plt.figure()
		ax = fig.add_subplot(111,projection='3d')
		for x in simple_world.photons:
			if not x.tracked:
				print("[+] found a dead one!")
				vec = x._vx,x._vy,x._vz
				plot_vector(ax,vec)
		plt.show()

	elif showoff == 3:
		fig = plt.figure()
		ax = fig.add_subplot(111)
		data = []
		TOTAL_LEN = len(simple_world.photons)
		LOST_LEN = 0
		for x in simple_world.photons:
			if not x.tracked:
				vec = x._vx,x._vy,x._vz
				data.append(acos(dotprod(vec,initial_velocity)/(norm(vec)*norm(initial_velocity))))
				LOST_LEN+=1
		plot_dist(ax,data)
		fp = open("report.txt","w")
		REPORT = "TOTAL: %d\nLOST: %d\nABSORBTION RATIO: %f\n"%(TOTAL_LEN,LOST_LEN,float(TOTAL_LEN-LOST_LEN)/TOTAL_LEN)
		fp.write(REPORT)
		fp.close()
		plt.show()

	elif showoff == 0:
		fp = open("world","w")
		pickle.dump(simple_world,fp)
		fp.close()

graphite(3)