def substitute(line):
	ret = line
	for i in range(2,12):
		ret = ret.replace(str(i),str(i)+".0")
	ret = ret.replace(".0sqrt",".0 * sqrt")
	ret = ret.replace("a","*a")
	return ret

fp = open("diamond.txt","r")
data = fp.readlines()

data = [substitute(x) for x in data]

fq = open("diamond.txt","w")
fq.write("".join(data))
fq.close()
fp.close()
