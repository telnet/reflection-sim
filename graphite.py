d= 1.0 # 핵간 거리   
x= 10  # 너비 스케일
y= 2   # 층수
h= 5   # 층간 거리 
l = []
for k in range(y):
    for i in range(x):
        for j in list(set(range(0,x))-set(range(2, x, 3))):
            a=(3**(1/2)*i*d,j*d,h*k*d)
            b=((i+1/2)*3**(1/2)*d,(3/2+j)*d,h*k*d)
            c=(3**(1/2)*i*d,j*d+d,(1/2+k)*h*d)
            e=((i+1/2)*3**(1/2)*d,(j-1/2)*d,(1/2+k)*h*d)
            l.append('(3**(1/2)*{}*d,{}*d,h*{}*d)'.format(i,j,k))
            l.append('(({}+1/2)*3**(1/2)*d,(3/2+{})*d,h*{}*d)'.format(i,j,k))
            l.append('(({}+1/2)*3**(1/2)*d,(3/2+{})*d,h*{}*d)'.format(i,j,k))
            l.append('(({}+1/2)*3**(1/2)*d,({}-1/2)*d,(1/2+{})*h*d)'.format(i,j,k))

f = open("graphite.txt", "w")

for x in l:
    f.write(str(x) + "\n")

f.close()
#print(l)