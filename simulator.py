from math import sqrt,pi,sin,cos
import random
TIME_UNIT = 0.0001
PHOTON_VELOCITY = 10
DT = TIME_UNIT
DIST_BIAS = 0.1
RANDOM_SCALE = 100000

cnt = 0

def norm(vector):
	val = 0
	for x in vector:
		val+=x**2
	return sqrt(val)

class World:
	def __init__(self,xsize,ysize,zsize,photons,atoms):
		'''
        properties :
            - xsize, ysize, zsize
                The size of each axis
                The original point is (0, 0, 0)
            - photons
                list of photon instances
            - atoms
                list of photon instances
            - time
                Times of iterations
        '''
		self.xsize = xsize
		self.ysize = ysize
		self.zsize = zsize
		self.photons = photons
		self.atoms = atoms
		self.time = 0

	def create_photon(self,new_photon):
		self.photons.append(new_photon)

	def delete_photon(self,photon):
		for x in self.photons:
			if x.uid == photon.uid:
				x.stop_tracking()
				return True
		return False

	def replace_photon(self,old_photon,new_photon):
		for i,x in enumerate(self.photons):
			if x.uid == old_photon.uid:
				self.photons[i] = new_photon
				new_photon.location_log = old_photon.location_log
				del old_photon
				return True
		return False

	def create_atom(self,new_atom):
		self.atoms.append(new_atom)

	def inct(self):
		self.move_photons()
		self.sanitize()
		self.time+=TIME_UNIT

	def sanitize(self):
		for photon in self.photons:
			if photon._x>self.xsize or photon._x<-self.xsize:
				self.delete_photon(photon)
			elif photon._y>self.ysize or photon._y<-self.ysize:
				self.delete_photon(photon)
			elif photon._z>self.zsize or photon._z<-self.zsize:
				self.delete_photon(photon)

	def move_photons(self):
		global cnt
		for photon in self.photons:
			if not photon.tracked:
				continue
			_x,_y,_z = photon.move()
			for atom in self.atoms:
				if atom.in_bound(_x,_y,_z):
					#########
					#print(cnt)
					#cnt += 1
					atom.absorb_photon(photon)
					break

	def stop_sim(self):
		for x in self.photons:
			x.stop_tracking()
					
class Photon:
	def __init__(self, world, loc_init, vel_init):
		self.world = world
		self.uid = len(self.world.photons)
		self._x, self._y, self._z = loc_init
		self._vx, self._vy, self._vz = vel_init
		self.location_log = []
		self.tracked = True
		

	def move(self):
		if self.tracked == False:
			return
		
		self._x+=DT*self._vx
		self._y+=DT*self._vy
		self._z+=DT*self._vz
		self.location_log.append((self._x,self._y,self._z))

		return self.location_log[-1]

	def stop_tracking(self):
		self.tracked = False
		self.last_speed = (self._vx,self._vy,self._vz)


class Atom:
	def __init__(self, world, loc_init,radius):
		self.world = world
		self.uid = len(self.world.atoms)
		self._x, self._y, self._z = loc_init
		self.radius = radius

	def in_bound(self,x,y,z):
		return norm([x-self._x,y-self._y,z-self._z])<self.radius

	def absorb_photon(self,photon):
		assert self.in_bound(photon._x,photon._y,photon._z)
		phi = pi * (float(random.randint(0,RANDOM_SCALE-1))/RANDOM_SCALE)
		theta = 2* pi * (float(random.randint(0,RANDOM_SCALE-1))/RANDOM_SCALE)
		loc_init = (self.radius + DIST_BIAS)*sin(theta)*cos(phi), (self.radius + DIST_BIAS)*sin(theta)*sin(phi), (self.radius + DIST_BIAS)*cos(theta)
		dx,dy,dz = loc_init
		loc_init = dx+photon._x, dy+photon._y, dz+photon._z
		random_vector = (PHOTON_VELOCITY)*sin(theta)*cos(phi), (PHOTON_VELOCITY)*sin(theta)*sin(phi), (PHOTON_VELOCITY)*cos(theta)
		new_photon = Photon(self.world,loc_init,random_vector)
		self.world.replace_photon(photon,new_photon)



